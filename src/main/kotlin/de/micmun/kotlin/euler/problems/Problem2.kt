/*
 * Problem2.kt
 *
 * Copyright 2019 by MicMun
 */

package de.micmun.kotlin.euler.problems

/**
 * Problem 2: By considering the terms in the Fibonacci sequence whose values
 * do not exceed four million, find the sum of the even-valued terms.
 *
 * @author MicMun
 * @version 1.0, 12.08.17
 */

class Problem2(val maxNumber: Int) {
   private fun fib(n1: Int, n2: Int): List<Int> {
      val numbers: MutableList<Int> = mutableListOf()
      var nr1: Int = n1
      var nr2: Int = n2

      while (nr1 + nr2 < maxNumber) {
         val f: Int = nr1 + nr2
         numbers.add(f)
         nr1 = nr2
         nr2 = f
      }

      return numbers
   }

   /**
    * Returns the sum of the even-valued numbers of fibonacci below maxNumber.
    *
    * @return sum of numbers.
    */
   fun getLoesung(): Int {
      val fibs: List<Int> = fib(0, 1)
      val sum: Int = (0 until fibs.size).sumBy {
         if (fibs[it].rem(2) == 0) fibs[it] else 0
      }

      return sum
   }

   /**
    * Returns the name of the problem class.
    *
    * @return name of the problem class.
    */
   override fun toString(): String {
      return "Problem2 - By considering the terms in the Fibonacci sequence " +
            "whose values do not exceed four million, find the sum of the" +
            " even-valued terms:"
   }
}