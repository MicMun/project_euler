/*
 * Problem3.kt
 *
 * Copyright 2019 by MicMun
 */

package de.micmun.kotlin.euler.problems

/**
 * Problem 3: What is the largest prime factor of the number 600851475143.
 *
 * @author MicMun
 * @version 1.0, 12.08.17
 */

class Problem3(private val number: Long) {
   /**
    * Returns the largest prime factor of the number 600851475143.
    *
    * @return sum of numbers.
    */
   fun getLoesung(): Long {
      var n: Double = number.toDouble()

      var d = 3.0
      var rt: Double

      if (n < 2)
         return -1

      while (n.rem(2) == 0.0)
         n /= 2

      if (n == 1.0)
         return 2

      rt = Math.sqrt(n)

      while (d <= rt) {
         if (n.rem(d) == 0.0) {
            n /= d;
            rt = Math.sqrt(n)
            continue
         }
         d += 2
      }

      return n.toLong()
   }

   /**
    * Returns the name of the problem class.
    *
    * @return name of the problem class.
    */
   override fun toString(): String {
      return "Problem3 - What is the largest prime factor of the number " +
            "600851475143:"
   }
}