/*
 * Problem7.kt
 *
 * Copyright 2019 by MicMun
 */

package de.micmun.kotlin.euler.problems

/**
 * Problem 7: What is the 10 001st prime number?.
 *
 * @author MicMun
 * @version 1.0, 18.08.17
 */

class Problem7(private val number: Int) {
   private val primes: MutableList<Int> = mutableListOf()

   /**
    * Returns <code>true</code>, if n is prime.
    *
    * @return <code>true</code>, if n is prime.
    */
   private fun isPrime(n: Int): Boolean {
      return (2..Math.sqrt(n.toDouble()).toInt()).none { n.rem(it) == 0 }
   }

   /**
    * Returns the 10 001st prime number.
    *
    * @return the 10 001st prime number.
    */
   fun getLoesung(): Int {
      var index = 1
      var zahl = 3

      primes.add(2)

      while (index != number) {
         if (isPrime(zahl)) {
            primes.add(zahl)
            index++
         }
         zahl += 2
      }

      return primes.last()
   }

   /**
    * Returns the name of the problem class.
    *
    * @return name of the problem class.
    */
   override fun toString(): String {
      return "Problem7 - What is the 10 001st prime number:"
   }
}