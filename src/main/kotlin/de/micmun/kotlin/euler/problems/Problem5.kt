/*
 * Problem5.kt
 *
 * Copyright 2019 by MicMun
 */

package de.micmun.kotlin.euler.problems

/**
 * Problem 5: What is the smallest positive number that is evenly divisible
 * by all of the numbers from 1 to 20?
 *
 * @author MicMun
 * @version 1.0, 18.08.17
 */

class Problem5(private val number: Int) {
   /**
    * Returns the smallest positive number that is evenly divisible
    * by all of the numbers from 1 to 20.
    *
    * @return smallest positive number.
    */
   fun getLoesung(): Int {
      var result = 1

      for (j in 1..number) {
         if (result.rem(j) > 0) {
            for (k in 1..number) {
               if ((result * k).rem(j) == 0) {
                  result *= k
                  break
               }
            }
         }
      }

      return result
   }

   /**
    * Returns the name of the problem class.
    *
    * @return name of the problem class.
    */
   override fun toString(): String {
      return "Problem5 - What is the smallest positive number that is " +
            "evenly divisible by all of the numbers from 1 to 20:"
   }
}