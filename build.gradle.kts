import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
   kotlin("jvm") version "1.3.41"
   application
}

group = "de.micmun.kotlin"
version = "1.0"

repositories {
   mavenCentral()
}

dependencies {
   implementation(kotlin("stdlib-jdk8"))
}

tasks.withType<KotlinCompile> {
   kotlinOptions.jvmTarget = "11"
}

application {
   mainClassName = "de.micmun.kotlin.euler.MainKt"
}
